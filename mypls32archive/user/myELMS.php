<?php
/*
 * e-academy ELMS Integrated User Verification
 */
 
 require_once('../config.php');
    require_once($CFG->libdir.'/gdlib.php');
    require_once($CFG->libdir.'/adminlib.php');
    require_once($CFG->dirroot.'/user/editadvanced_form.php');
    require_once($CFG->dirroot.'/user/editlib.php');
    require_once($CFG->dirroot.'/user/profile/lib.php');
	require_once($CFG->dirroot.'/lib/formslib.php');
	
	$PAGE->https_required();
	
    $id     = optional_param('id', $USER->id, PARAM_INT);    // user id; -1 if creating new user
    $course = optional_param('course', SITEID, PARAM_INT);   // course id (defaults to Site)
	global $DB,$course,$id;
	if (!$course = $DB->get_record('course', array('id' => $course))) {
        error('Course ID was incorrect');
    }
    require_login($course->id);

    if ($course->id == SITEID) {
        $coursecontext = get_context_instance(CONTEXT_SYSTEM);   // SYSTEM context
    } else {
        $coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);   // Course context
    }
	
	$systemcontext = get_context_instance(CONTEXT_SYSTEM);

    //load user preferences
    useredit_load_preferences($user);

    //Load custom profile fields data
    profile_load_data($user);

    //user interests separated by commas
    if (!empty($CFG->usetags)) {
        require_once($CFG->dirroot.'/tag/lib.php');
        $user->interests = tag_get_tags_csv('user', $id, TAG_RETURN_TEXT); // formslib uses htmlentities itself
    }

session_start();
	
	
			$userfullname = fullname($user, true);
			$passwd = fullname($user, true);
			$mail = $USER->email;
			$firstname = $USER->firstname;
			$lastname =  $USER->lastname;
			$username =  $USER->username;
			
		
			//if ($sictstudent == 1){
			//assigning the parameter values
			$_SESSION['auth_ccid'] = $username;
			$_SESSION['account_number'] = '100062180';
			$_SESSION['auth_email'] = $mail;
			$_SESSION['first_name'] = $firstname;
			$_SESSION['last_name'] = $lastname;
			$_SESSION['academic_statuses'] = '';
			
			global $COURSE, $USER;

			$context = get_context_instance(CONTEXT_COURSE,$COURSE->id);
			$roles = get_user_roles($context, $USER->id, true);
			
			if (user_has_role_assignment($USER->id,5)) {
			$_SESSION['academic_statuses'] = 'students';
			}
			elseif (user_has_role_assignment($USER->id,4))  {
			$_SESSION['academic_statuses'] = 'staff';
			}
			elseif (user_has_role_assignment($USER->id,3)) {
			$_SESSION['academic_statuses'] = 'faculty';
			}
			elseif (user_has_role_assignment($USER->id,1)) {
			$_SESSION['academic_statuses'] = 'staff,students,faculty';
			}
			else 
			{
				$_SESSION['academic_statuses'] = '';
			}

			$_error = '';
			
			//}
			
			session_write_close();
			
		
// user has been authenticated - handshake
if (isset($_SESSION['auth_ccid'])) 
{
	
	ob_start();
	
	
	$key = 'b7196450'; // replace with your webstore key
	
	$host = 'https://e5.onthehub.com/WebStore/Security/AuthenticateUser.aspx';
	
	// build e5 verification page query string variables
	$vars = 'username='.urlencode($_SESSION['auth_ccid']);
	$vars .= '&account='.urlencode($_SESSION['account_number']);
	$vars .= '&email='.urlencode($_SESSION['auth_email']);
	$vars .= '&first_name='.urlencode($_SESSION['first_name']);
	$vars .= '&last_name='.urlencode($_SESSION['last_name']);
	$vars .= '&academic_statuses='.urlencode($_SESSION['academic_statuses']);
	$vars .= '&shopper_ip='.urlencode($_SERVER['REMOTE_ADDR']);
	$vars .= '&key='.urlencode($key);

	$e5verfurl = $host.'?'.$vars;	
	
	$handle = fopen($e5verfurl,'r');
	$e5LoginRedirectURL = stream_get_contents($handle);
	fclose($handle);
	//echo $e5verfurl;
	
	// check response status code
	$http_status = $http_response_header[0];
	if (strpos($http_status, '200 OK') === false)	// NOTE $http_status may not always be $http_response_header[0]
	{
		// we have an error
		echo '<p><b>A handshake error has occured</b></p>';
		echo '<p><b>Response received:<br><font color="red">'.$http_status.'</font></b></p>';
	}
	else if (strlen($e5LoginRedirectURL) == 0)
	{
		// HTTP status code was OK but server didn't return a redirect URL; may be some other error, such as incorrectly configured server IP for your server
		echo '<p><b>A handshake error has occured; invalid redirection URL</b></p>';
	}
	else 
	{
		// status code looks good and we have a redirection URL; set redirect location in header
		header('Location: '.$e5LoginRedirectURL);
	}
/*
	// clear session variables and destroy // disable coz currently running on Moodle 1.9, moodle need the cookies.
	$_SESSION=array();
	if(isset($_COOKIE[session_name()])) 
	{
		setcookie(session_name(),'',time()-42000,'/');
	}
	@session_destroy();
	ob_end_flush();
	exit;
	*/
	
}

// unauthenticated user - show login
?>