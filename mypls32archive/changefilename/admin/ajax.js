function doAjaxUpdater(objForm, url, target, actionName) {
    var theForm = objForm;
	var pars = '_d=d';
	var count = theForm.elements.length;
	theForm.command.value = actionName;
	for ( var i=0; i < count; i++){
	   var id = theForm.elements[i].id;
	   var name = theForm.elements[i].name;
	   if ( name == '' ) theForm.elements[i].name = id;
	   if ( id == '' ) theForm.elements[i].id = name;
       name = theForm.elements[i].name;
	   var value = theForm.elements[i].value;
       pars = pars + '&' + name + '=' + escape(value);
	}
    var myAjax = new Ajax.Updater(target, url, {method: 'post', parameters: pars});		 
}

function doAjaxUpdater(objForm, url, target, actionName, qs) {
    var theForm = objForm;
	var pars = '_d=d';
	var count = theForm.elements.length;
	theForm.command.value = actionName;
	var readparam = false;
	
	for ( var i=0; i < count; i++){
	   readparam = false;
	   if ( theForm.elements[i].type == "checkbox" ) {
		  if ( theForm.elements[i].checked ) {
		     readparam = true;
		  } 
	   } else {
	   	  readparam = true;
	   }
	  
	   if ( readparam ) {
	  	   var id = theForm.elements[i].id;
		   var name = theForm.elements[i].name;
		   if ( name == '' ) theForm.elements[i].name = id;
		   if ( id == '' ) theForm.elements[i].id = name;
	       name = theForm.elements[i].name;
		   var value = theForm.elements[i].value;
	       pars = pars + '&' + name + '=' + escape(value);
       }
	}
	pars = pars + '&' + qs;

    var myAjax = new Ajax.Updater(target, url, {method: 'post', parameters: pars});		
      
}

function numeralsOnly(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : 
        ((evt.which) ? evt.which : 0));
    if ( charCode == 46 || charCode== 45) return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function admission_apply_step_1(){
	
	    popWin = window.open("", "Window3", "width=400,height=500,scrollbars=yes,location=no"); 
		popWin.document.writeln('<bgcolor="red"><h4>Admission</h4></bgcolor>');
		popWin.document.writeln('<table><tr><td><b>Description :</b></td></tr>');
		popWin.document.writeln('<tr><td><small>This is the module for administrator which is contains all the information that applicants fill in. This module included 6 part which is Application Form, Application Status, Student Registration, Student Registration (Executive) and Student Registration (COHORT). You will be able to edit or update the information in this module.</small></td></tr>');
		popWin.document.writeln('<tr><td><table cellspacing=4 cellpadding=4 valign=top><tr><td></td><td></td><td></td></tr>');
		popWin.document.writeln('<tr><td valign="top"></td><td valign="top"></td><td valign="top"><small><b><i>Application Form</i></b></small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step 1</td><td valign="top">:</td><td valign="top"><small>Select <i><b>Applicant Type </b></i> from the drop-down list.</small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step 2</td><td valign="top">:</td><td valign="top"><small>Select <b><i>Choice of Study </i></b> from the drop-down list.</small></td></tr>');	
		popWin.document.writeln('<tr><td valign="top">Step 3 </td><td valign="top">:</td><td valign="top"><small>Select <b><i>Preferred Intake </i></b> from the drop-down list.</small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step 4 </td><td valign="top">:</td><td valign="top"><small>Click <b><i>NEXT</i></b> button to proceed. </small></td></tr>');
		popWin.document.writeln('</table></td></tr>');
		
} 



function campusDirectory_viewStudent(){
	
		popWin = window.open("", "Window3", "width=400,height=500,scrollbars=no,location=no"); 
		popWin.document.writeln('<h4>View Students</h4>');
	  	popWin.document.writeln('<table><tr><td><b>Description :</b></td></tr>');
		popWin.document.writeln('<tr><td><small>In this module,details of AeU registered students can be viewed based on <b>School</b>, <b>Enrolled Programme</b> or/and <b>Intake</b>.</small></td></tr>');
		popWin.document.writeln('<tr><td><table cellspacing=4 cellpadding=4 valign=top><tr><td></td><td></td><td></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step1 </td><td valign="top">:</td><td valign="top"><small>Select <i><b>School</b></i> OR/AND <i><b>Programme</b></i> OR/AND <i><b>Intake</b></i>.</small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step2 </td><td valign="top">:</td><td valign="top"><small>Click "Submit" button.</small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Output </td><td valign="top">:</td><td valign="top"><small>A list of student records will be displayed.</small></td></tr>');
		popWin.document.writeln('</table></td></tr>');
		popWin.document.writeln('<tr><td>__________________________________</td></tr>');
		popWin.document.writeln('<tr><td><table>');
		popWin.document.writeln('<tr><td><b><small>Pre-condition</small></b></td></tr>');
		popWin.document.writeln('<tr><td><small>- NONE -</small></td></tr>');
		popWin.document.writeln('<tr><td><b><small>Post-condition</small></b></td></tr>');
		popWin.document.writeln('<tr><td><small>- NONE -</small></td></tr>');
		popWin.document.writeln('<tr><td>__________________________________</td></tr>');
		popWin.document.writeln('</table></td></tr></table>');
		
		
		popWin.document.location.enable = false;
		popWin.document.statusbar.enable = false;
	}
	
function campusDirectory_viewSchoolmates(){
	
		popWin = window.open("", "Window3", "width=400,height=400,scrollbars=no,location=no"); 
		popWin.document.writeln('<h4>View Schoolmates</h4>');
	  	popWin.document.writeln('<table><tr><td><b>Description :</b></td></tr>');
		popWin.document.writeln('<tr><td><small>In this module,details of the registered students within the same School can be viewed based on <b>Enrolled Programme</b> or/and <b>Intake</b>.</small></td></tr>');
		popWin.document.writeln('<tr><td><table cellspacing=4 cellpadding=4 valign=top><tr><td><td><td></td></td></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step1 </td><td valign="top">:</td><td valign="top"><small>Select <i><b>Programme</b></i> OR/AND <i><b>Intake</b></i>.</small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step2 </td><td valign="top">:</td><td valign="top"><small>Click "Submit" button.</small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Output </td><td valign="top">:</td><td valign="top"><small>Filtered records of the students within the same School will be displayed.</small></td></tr>');
		popWin.document.writeln('</table></td></tr>');
		popWin.document.writeln('<tr><td>__________________________________</td></tr>');
		popWin.document.writeln('<tr><td><table>');
		popWin.document.writeln('<tr><td><b><small>Pre-condition</small></b></td></tr>');
		popWin.document.writeln('<tr><td><small>- NONE -</small></td></tr>');
		popWin.document.writeln('<tr><td><b><small>Post-condition</small></b></td></tr>');
		popWin.document.writeln('<tr><td><small>- NONE -</small></td></tr>');
		popWin.document.writeln('<tr><td>__________________________________</td></tr>');
		popWin.document.writeln('</table></td></tr></table>');
		
		
		popWin.document.location.enable = false;
		popWin.document.statusbar.enable = false;
	}
	
function campusDirectory_viewCoursemates(){
	
		popWin = window.open("", "Window3", "width=400,height=400,scrollbars=no,location=no"); 
		popWin.document.writeln('<h4>View Programme Mates</h4>');
	  	popWin.document.writeln('<table><tr><td><b>Description :</b></td></tr>');
		popWin.document.writeln('<tr><td><small>In this module,details of the registered students within the same Programme can be viewed based on <b>Intake</b>.</small></td></tr>');
		popWin.document.writeln('<tr><td><table cellspacing=4 cellpadding=4 valign=top><tr><td></td><td></td><td></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step1 </td><td valign="top">:</td><td><small>Select Intake from <i><b>My course mates in --- intake.</b></i> dropdown menu.</small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step2 </td><td valign="top">:</td><td><small>Click "Submit" button.</small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Output </td><td valign="top">:</td><td><small>Filtered records of registered students within the same Programme will be displayed.</small></td></tr>');
		popWin.document.writeln('</table></td></tr>');
		popWin.document.writeln('<tr><td>__________________________________</td></tr>');
		popWin.document.writeln('<tr><td><table>');
		popWin.document.writeln('<tr><td><b><small>Pre-condition</small></b></td></tr>');
		popWin.document.writeln('<tr><td><small>- NONE -</small></td></tr>');
		popWin.document.writeln('<tr><td><b><small>Post-condition</small></b></td></tr>');
		popWin.document.writeln('<tr><td><small>- NONE -</small></td></tr>');
		popWin.document.writeln('<tr><td>__________________________________</td></tr>');
		popWin.document.writeln('</table></td></tr></table>');
		
		
		popWin.document.location.enable = false;
		popWin.document.statusbar.enable = false;
	}


	function campusDirectory_viewClassmates(){
	
		popWin = window.open("", "Window3", "width=400,height=400,scrollbars=no,location=no"); 
		popWin.document.writeln('<h4>View Classmates</h4>');
	  	popWin.document.writeln('<table><tr><td><b>Description :</b></td></tr>');
		popWin.document.writeln('<tr><td><small>In this module,details of the registered students who enroll similar subjects will be displayed.</small></td></tr>');
		popWin.document.writeln('<tr><td><table cellspacing=4 cellpadding=4 valign=top><tr><td><u>Steps:</u></td><td></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step1 </td><td valign="top">:</td><td valign="top"><small>Select a subject from <i><b>My class mates for subject</b></i> dropdown menu.</small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step2 </td><td valign="top">:</td><td valign="top"><small>Click "Submit" button.</small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Output </td><td valign="top">:</td><td valign="top"><small>Records of registered students within the same class will be displayed.</small></td></tr>');
		popWin.document.writeln('</table></td></tr>');
		popWin.document.writeln('<tr><td>__________________________________</td></tr>');
		popWin.document.writeln('<tr><td><table>');
		popWin.document.writeln('<tr><td><b><small>Pre-condition</small></b></td></tr>');
		popWin.document.writeln('<tr><td><small>- NONE -</small></td></tr>');
		popWin.document.writeln('<tr><td><b><small>Post-condition</small></b></td></tr>');
		popWin.document.writeln('<tr><td><small>- NONE -</small></td></tr>');
		popWin.document.writeln('<tr><td>__________________________________</td></tr>');
		popWin.document.writeln('</table></td></tr></table>');
		
		
		popWin.document.location.enable = false;
		popWin.document.statusbar.enable = false;
	}

function campusDirectory_viewLecturer(){
	
		popWin = window.open("", "Window3", "width=400,height=500,scrollbars=no,location=no"); 
		popWin.document.writeln('<h4>View Lecturer</h4>');
	  	popWin.document.writeln('<table><tr><td><b>Description :</b></td></tr>');
		popWin.document.writeln('<tr><td><small>In this module, details of AeU lecturer can be viewed based on  <b>Lecturer</b>, <b>Group</b>, <b>Subject</b> or/and <b>Sort by</b>.</small></td></tr>');
		popWin.document.writeln('<tr><td><table cellspacing=4 cellpadding=4 valign=top><tr><td></td><td></td><td></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step1 </td><td valign="top">:</td><td valign="top"><small>Select <i><b>Lecturer</b></i> OR/AND <i><b>Group</b></i> OR/AND <i><b>Subject</b></i> OR/AND <i><b>Sort by</b></i>.</small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Step2 </td><td valign="top">:</td><td valign="top"><small>Click "Submit" button.</small></td></tr>');
		popWin.document.writeln('<tr><td valign="top">Output </td><td valign="top">:</td><td valign="top"><small>A list of lecturer records will be displayed.</small></td></tr>');
		popWin.document.writeln('</table></td></tr>');
		popWin.document.writeln('<tr><td>__________________________________</td></tr>');
		popWin.document.writeln('<tr><td><table>');
		popWin.document.writeln('<tr><td><b><small>Pre-condition</small></b></td></tr>');
		popWin.document.writeln('<tr><td><small>- NONE -</small></td></tr>');
		popWin.document.writeln('<tr><td><b><small>Post-condition</small></b></td></tr>');
		popWin.document.writeln('<tr><td><small>- NONE -</small></td></tr>');
		popWin.document.writeln('<tr><td>__________________________________</td></tr>');
		popWin.document.writeln('</table></td></tr></table>');
		
		
		popWin.document.location.enable = false;
		popWin.document.statusbar.enable = false;
	}
	