<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>

<head>

<title>View Records</title>

</head>

<body>



<?php

/*

VIEW-PAGINATED.PHP

Displays all data from 'players' table

This is a modified version of view.php that includes pagination

*/



// connect to the database

include('connect-db.php');



// number of results to show per page

$per_page = 3;



// figure out the total pages in the database

$result = mysql_query("SELECT * FROM vAlumniBusiness");

$total_results = mysql_num_rows($result);

$total_pages = ceil($total_results / $per_page);



// check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)

if (isset($_GET['page']) && is_numeric($_GET['page']))

{

$show_page = $_GET['page'];



// make sure the $show_page value is valid

if ($show_page > 0 && $show_page <= $total_pages)

{

$start = ($show_page -1) * $per_page;

$end = $start + $per_page;

}

else

{

// error - show first set of results

$start = 0;

$end = $per_page;

}

}

else

{

// if page isn't set, show first set of results

$start = 0;

$end = $per_page;

}



// display pagination



echo "<p><a href='view.php'>View All</a> | <b>View Page:</b> ";

for ($i = 1; $i <= $total_pages; $i++)

{

echo "<a href='view-paginated.php?page=$i'>$i</a> ";

}

echo "</p>";



// display data in table

echo "<table border='1' cellpadding='10'>";

echo "<tr> <th>NAME</th> <th>COMPANY_NAME</th> <th>BUSINESS_DESC</th> <th>PHONE</th> <th>EMAIL</th> <th>COMPANY_WEBSITE</th> <th>BUSINESS_WEBSITE</th> <th>Industry</th> <th>STREET</th> <th>CITY</th><th></th></tr>";



// loop through results of database query, displaying them in the table

for ($i = $start; $i < $end; $i++)

{

// make sure that PHP doesn't try to show results that don't exist

if ($i == $total_results) { break; }



// echo out the contents of each row into a table

echo "<tr>";

echo '<td>' . mysql_result($result, $i, 'NAME') . '</td>';

echo '<td>' . mysql_result($result, $i, 'COMPANY_NAME') . '</td>';

echo '<td>' . mysql_result($result, $i, 'BUSINESS_DESC') . '</td>';

echo '<td>' . mysql_result($result, $i, 'PHONE') . '</td>';

echo '<td>' . mysql_result($result, $i, 'EMAIL') . '</td>';

echo '<td>' . mysql_result($result, $i, 'COMPANY_WEBSITE') . '</td>';

echo '<td>' . mysql_result($result, $i, 'BUSINESS_WEBSITE') . '</td>';

echo '<td>' . mysql_result($result, $i, 'Industry') . '</td>';

echo '<td>' . mysql_result($result, $i, 'STREET') . '</td>';

echo '<td>' . mysql_result($result, $i, 'CITY') . '</td>';



echo '<td><a href="delete.php?id=' . mysql_result($result, $i, 'ID') . '">Delete</a></td>';

echo "</tr>";

}

// close table>

echo "</table>";



// pagination



?>

<!--p><a href="new.php">Add a new record</a></p-->



</body>

</html>