<?php

/*

EDIT.PHP

Allows user to edit specific entry in database

*/



// creates the edit record form

// since this form is used multiple times in this file, I have made it a function that is easily reusable

function renderForm($id,$NAME, $COMPANY_NAME, $BUSINESS_DESC, $PHONE,$EMAIL,$COMPANY_WEBSITE,$BUSINESS_WEBSITE,$Industry,$STREET,$CITY, $error)

{

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>

<head>

<title>Edit Record</title>

</head>

<body>

<?php

// if there are any errors, display them

if ($error != '')

{

echo '<div style="padding:4px; border:1px solid red; color:red;">'.$error.'</div>';

}

?>



<form action="" method="post">

<input type="hidden" name="id" value="<?php echo $id; ?>"/>

<div>

<p><strong>NAME:</strong> <?php echo $NAME; ?></p>

<strong>COMPANY_NAME: *</strong> <input type="text" name="COMPANY_NAME" value="<?php echo $COMPANY_NAME; ?>" disabled/><br/>

<strong>BUSINESS_DESC: *</strong> <input type="text" name="BUSINESS_DESC" value="<?php echo $BUSINESS_DESC; ?>"/><br/>

<strong>PHONE: *</strong> <input type="text" name="PHONE" value="<?php echo $PHONE; ?>" disabled/><br/>

<strong>EMAIL: *</strong> <input type="text" name="EMAIL" value="<?php echo $EMAIL; ?>" disabled/><br/>

<strong>COMPANY_WEBSITE: *</strong> <input type="text" name="COMPANY_WEBSITE" value="<?php echo $COMPANY_WEBSITE; ?>" disabled/><br/>

<strong>BUSINESS_WEBSITE: *</strong> <input type="text" name="BUSINESS_WEBSITE" value="<?php echo $BUSINESS_WEBSITE; ?>" disabled/><br/>

<strong>Industry: *</strong> <input type="text" name="Industry" value="<?php echo $Industry; ?>" disabled/><br/>

<strong>STREET: *</strong> <input type="text" name="STREET" value="<?php echo $STREET; ?>" disabled/><br/>

<strong>CITY: *</strong> <input type="text" name="CITY" value="<?php echo $CITY; ?>" disabled/><br/>
<p>* Required</p>

<input type="submit" name="submit" value="Submit">

</div>

</form>

</body>

</html>

<?php

}







// connect to the database

include('connect-db.php');



// check if the form has been submitted. If it has, process the form and save it to the database

if (isset($_POST['submit']))

{

// confirm that the 'id' value is a valid integer before getting the form data

if (is_numeric($_POST['id']))

{

// get form data, making sure it is valid

$id = $_POST['id'];

$COMPANY_NAME = mysql_real_escape_string(htmlspecialchars($_POST['COMPANY_NAME']));

$BUSINESS_DESC = mysql_real_escape_string(htmlspecialchars($_POST['BUSINESS_DESC']));

$PHONE = mysql_real_escape_string(htmlspecialchars($_POST['PHONE']));

$EMAIL = mysql_real_escape_string(htmlspecialchars($_POST['EMAIL']));

$COMPANY_WEBSITE = mysql_real_escape_string(htmlspecialchars($_POST['COMPANY_WEBSITE']));

$BUSINESS_WEBSITE = mysql_real_escape_string(htmlspecialchars($_POST['BUSINESS_WEBSITE']));

$Industry = mysql_real_escape_string(htmlspecialchars($_POST['Industry']));

$STREET = mysql_real_escape_string(htmlspecialchars($_POST['STREET']));

$CITY = mysql_real_escape_string(htmlspecialchars($_POST['CITY']));



// check that firstname/lastname fields are both filled in

if ($MSG == '')

{

// generate error message

$error = 'ERROR: Please fill in all required fields!';



//error, display form

renderForm($id, $COMPANY_NAME, $BUSINESS_DESC, $PHONE, $EMAIL, $COMPANY_WEBSITE, $BUSINESS_WEBSITE, $Industry, $STREET, $CITY, $error);

}

else

{

// save the data to the database

mysql_query("UPDATE FORUM SET MSG='$MSG' WHERE ID='$id'")

or die(mysql_error());



// once saved, redirect back to the view page

header("Location: view.php");

}

}

else

{

// if the 'id' isn't valid, display an error

echo 'Error!';

}

}

else

// if the form hasn't been submitted, get the data from the db and display the form

{



// get the 'id' value from the URL (if it exists), making sure that it is valid (checing that it is numeric/larger than 0)

if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0)

{

// query db

$id = $_GET['id'];

$result = mysql_query("SELECT * FROM FORUM WHERE ID=$id")

or die(mysql_error());

$row = mysql_fetch_array($result);



// check that the 'id' matches up with a row in the databse

if($row)

{



// get data from db

$CATEGORY_ID = $row['CATEGORY_ID'];

$USER_ID = $row['USER_ID'];

$MSG = $row['MSG'];

$DATE_CREATED = $row['DATE_CREATED'];



// show form

renderForm($id, $CATEGORY_ID, $USER_ID, $MSG, $DATE_CREATED, '');

}

else

// if no match, display result

{

echo "No results!";

}

}

else

// if the 'id' in the URL isn't valid, or if there is no 'id' value, display an error

{

echo 'Error!';

}

}

?>