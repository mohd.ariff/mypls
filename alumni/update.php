
<script>
     require(["dojo/dom", "dojo/query","dojo/on","dojo/dom-construct", "cms/Util", 
              "dojo/NodeList-dom", "dojo/NodeList-data", "bootstrap/Modal", "dojo/domReady!"],
         function (dom, query, on, domConstruct, util) {
    	 	<% if (!new AlumniService().hasUpdatePreferredEmail()){ %>
      			query('#alumniUpdateEmailDlg').modal();
      		<% } %>
      		
      		var textEmail = dom.byId('alumniPreferredEmail');
      		var textPassword = dom.byId('alumniPassword');
      		var textPasswordRetype = dom.byId('alumniPasswordRetype');
      		var btnSubmit = dom.byId('btnSubmitAlumniPreferredEmail');
      		var btnCancel = dom.byId('btnCancelAlumniPreferredEmail');
      		
      		on(btnSubmit, 'click', function(){
      			if (validate()){
      				var email = textEmail.value.trim();
      				var password = textPassword.value.trim();
      				submitCredential(email, password);
      			}
      		});
      		
      		on(btnCancel, 'click', function(){
      			util.openUrl(cmsConfig.contextPath + '/logout');
      		});
      		
      		function validate(){
      			var email = textEmail.value.trim();
      			var password = textPassword.value.trim();
      			var repassword = textPasswordRetype.value.trim();
      			if (email.length==0){
      				alert('Please enter your preferred email');
      				textEmail.focus();
      				return false;
      			}
      			if (!util.isValidEmail(email)){
      				alert('Invalid email');
      				textEmail.focus();
      				return false;
      			}
      			if (util.hasWhiteSpace(password)){
					alert("Password cannot contain whitespace");
					return false;
				}
				if (password.length<6){
					alert("Password must contain 6 characters or more");
					return false;
				}
				if (password!=repassword){
					alert("Password and Retype password \nmust be identical");
					return false;
				}
      			return true;
      		}
      		
      		function submitCredential(email, password){
      			$.jsonRPC.setup({
					  endPoint: cmsConfig.contextPath + '/jsonrpc/alumniService'
				});
				util.showLoadingSpinner();
				$.jsonRPC.request('updateCredential', {
					params: {email: email, password: password},
				 	success: function() {
								//if no error refresh the page
				 				window.location.href = "";
							},
					error: function (e){
						util.hideLoadingSpinner();
						console.log(e);
						if (e.error instanceof Object){
							if (e.error.message=="DuplicateIdException"){ //specific no session detected
								alert("Email already used by other user");
								return;
							}
							if (e.error.message=="PasswordNotChangeException"){ //specific no session detected
								alert("New password must be different than current");
								return;
							}
						}
						util.handleRpcError(e);	
					}
				});
      		}
      		
      		function resetForm(){
      			textEmail.value = "";
      		}
      		
      		
         }
     );
 </script>

<div class="modal fade" id="alumniUpdateEmailDlg" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body">
				
				<form>
					<h4>Preferred email address</h4>
					<div class="form-group">
						<input type="email"
							class="form-control" id="alumniPreferredEmail" placeholder="Email" maxlength="50">
					</div>
					<div class="form-group">
						<div class="alert alert-info">
						  This email shall be used as your login ID and all notification (if any) shall be sent using the email.
						</div>
					</div>
					<h4>Update password</h4>
					<div class="form-group">
						<input type="password"
							class="form-control" id="alumniPassword" placeholder="new password" maxlength="20">
					</div>
					<div class="form-group">
						<input type="password"
							class="form-control" id="alumniPasswordRetype" placeholder="retype password" maxlength="20">
					</div>
					<div class="row">
						<div class="col-sm-6">
							<button id="btnSubmitAlumniPreferredEmail" type="button"
								class="btn btn-primary btn-md btn-block"
								style="margin-top: 5px; margin-bottom: 5px">Submit</button>
						</div>
						<div class="col-sm-6">
							<button id="btnCancelAlumniPreferredEmail" type="button" class="btn btn-danger btn-md btn-block"
								style="margin-top: 5px; margin-bottom: 5px">Cancel</button>
						</div>
					</div>
				</form>
			</div>
			
		</div>
	</div>
</div>