<?php

/*

EDIT.PHP

Allows user to edit specific entry in database

*/



// creates the edit record form

// since this form is used multiple times in this file, I have made it a function that is easily reusable

function renderForm($id,$CATEGORY_ID, $USER_ID, $MSG, $DATE_CREATED, $error)

{

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>

<head>

<title>Edit Record</title>

</head>

<body>

<?php

// if there are any errors, display them

if ($error != '')

{

echo '<div style="padding:4px; border:1px solid red; color:red;">'.$error.'</div>';

}

?>



<form action="" method="post">

<input type="hidden" name="id" value="<?php echo $id; ?>"/>

<div>

<p><strong>CATEGORY_ID:</strong> <?php echo $CATEGORY_ID; ?></p>

<strong>USER_ID: *</strong> <input type="text" name="USER_ID" value="<?php echo $USER_ID; ?>" disabled/><br/>

<strong>MSG: *</strong> <input type="text" name="MSG" value="<?php echo $MSG; ?>"/><br/>

<strong>DATE_CREATED: *</strong> <input type="text" name="MSG" value="<?php echo $DATE_CREATED; ?>" disabled/><br/>

<p>* Required</p>

<input type="submit" name="submit" value="Submit">

</div>

</form>

</body>

</html>

<?php

}







// connect to the database

include('connect-db.php');



// check if the form has been submitted. If it has, process the form and save it to the database

if (isset($_POST['submit']))

{

// confirm that the 'id' value is a valid integer before getting the form data

if (is_numeric($_POST['id']))

{

// get form data, making sure it is valid

$id = $_POST['id'];

$CATEGORY_ID = mysql_real_escape_string(htmlspecialchars($_POST['CATEGORY_ID']));

$USER_ID = mysql_real_escape_string(htmlspecialchars($_POST['USER_ID']));

$MSG = mysql_real_escape_string(htmlspecialchars($_POST['MSG']));

$DATE_CREATED = mysql_real_escape_string(htmlspecialchars($_POST['DATE_CREATED']));



// check that firstname/lastname fields are both filled in

if ($MSG == '')

{

// generate error message

$error = 'ERROR: Please fill in all required fields!';



//error, display form

renderForm($id, $CATEGORY_ID, $USER_ID, $MSG, $DATE_CREATED, $error);

}

else

{

// save the data to the database

mysql_query("UPDATE FORUM SET MSG='$MSG' WHERE ID='$id'")

or die(mysql_error());



// once saved, redirect back to the view page

header("Location: view.php");

}

}

else

{

// if the 'id' isn't valid, display an error

echo 'Error!';

}

}

else

// if the form hasn't been submitted, get the data from the db and display the form

{



// get the 'id' value from the URL (if it exists), making sure that it is valid (checing that it is numeric/larger than 0)

if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0)

{

// query db

$id = $_GET['id'];

$result = mysql_query("SELECT * FROM FORUM WHERE ID=$id")

or die(mysql_error());

$row = mysql_fetch_array($result);



// check that the 'id' matches up with a row in the databse

if($row)

{



// get data from db

$CATEGORY_ID = $row['CATEGORY_ID'];

$USER_ID = $row['USER_ID'];

$MSG = $row['MSG'];

$DATE_CREATED = $row['DATE_CREATED'];



// show form

renderForm($id, $CATEGORY_ID, $USER_ID, $MSG, $DATE_CREATED, '');

}

else

// if no match, display result

{

echo "No results!";

}

}

else

// if the 'id' in the URL isn't valid, or if there is no 'id' value, display an error

{

echo 'Error!';

}

}

?>