<?php


$aid 	= $_GET["aid"];
$uid 	= $_GET["uid"];
$email  = $_GET["email"];
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<script>
var check = function() {
	
if(document.getElementById('password').value !=	"" && document.getElementById('confirm_password').value !="")
{
  if (document.getElementById('password').value ==
    document.getElementById('confirm_password').value) {
    document.getElementById('message').style.color = 'green';
    document.getElementById('message').innerHTML = 'Matched';
  } else 
  {
    document.getElementById('message').style.color = 'red';
    document.getElementById('message').innerHTML = 'Not Match';
  }
}
else
 {
    document.getElementById('message').style.color = 'red';
    document.getElementById('message').innerHTML = 'Not Match';
  }	
}

function check_pass() {
if(document.getElementById('password').value !=	"" && document.getElementById('confirm_password').value !="")
{	
    if (document.getElementById('password').value ==
            document.getElementById('confirm_password').value) {
        document.getElementById('submit').disabled = false;
    } else {
        document.getElementById('submit').disabled = true;
    }
}
else {
        document.getElementById('submit').disabled = true;
    }
}

function reset() {
    document.getElementById("reused_form").reset();
}

function checkField(fieldname)
{
if (/[A-Z]\S/.test(fieldname.value))
{
error = "Good";
return true;
}
else
{

document.getElementById("errorid").innerHTML = error;	
fieldname.value = "";
fieldname.focus();
return false;
}
}
</script>
    </head>
    <body >
	<script>
	</script>
        <div class="container">
            <!-- Form Started -->
            <div class="container form-top">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                        <div class="panel panel-danger">
                            <div class="panel-body">
                     <form id="reused_form" action="/form/process.php" method="post">
                                    					<h4>Email address</h4>
					<div class="form-group">
						<input type="email"
							class="form-control" id="email" name="email" value="<?php echo $email;?>" maxlength="50" disabled>
					</div>
					<div class="form-group">
					<input type="hidden" name="aid" value="<?php echo $aid;?>">
					</div>
					<div class="form-group">
					<input type="hidden" name="uid" value="<?php echo $uid;?>">
					</div>					
					<div class="form-group">
						<div class="alert alert-info">
						  This email shall be used as your login ID and all notification (if any) shall be sent using the email.
						</div>
					</div>
					<h4>Update password</h4>
					<p>Password must contain at least 1 alphanumeric character</p>
					<div class="form-group">
						
						<input type="password"
							class="form-control" id="password" name="password" placeholder="new password" maxlength="20" onkeyup='check();check_pass();' onblur="checkField(this)" required>
					</div>
					<div class="form-group">
						<input type="password"
							class="form-control" id="confirm_password" name="confirm_password" placeholder="retype password" maxlength="20" onkeyup='check();check_pass();' required>
							
					</div>
					<span id='message'></span>
					<div class="row">
						<div class="col-sm-6">
							<button id="submit" name="submit" type="submit"
								class="btn btn-primary btn-md btn-block"
								style="margin-top: 5px; margin-bottom: 5px" disabled>Submit</button>
						</div>
						<div class="col-sm-6">
							<button type="button" class="btn btn-danger btn-md btn-block"
								style="margin-top: 5px; margin-bottom: 5px" onclick='reset();' >Reset</button>
						</div>
					</div>
                                </form>
                                <div id="error_message" style="width:100%; height:100%; display:none; ">
                                    <h4>
                                        Error
                                    </h4>
                                    Sorry there was an error sending your form. 
                                </div>
                                <div id="success_message" style="width:100%; height:100%; display:none; ">
<h2>Success! Your Message was Sent Successfully.</h2>
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Form Ended -->
        </div>
    </body>
</html>

