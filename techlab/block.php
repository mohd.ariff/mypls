<html>
<body>

15th March 2016<br><br>

Dear Student,<br><br>

The earlier announcement dated 3rd March below refers.<br><br>

We would like to thank all students who have responded positively to the announcement by either making the necessary payment for<br>their outstanding fees or submitting the Letter of Undertaking (LOU 1/2016) to pay in instalments. In return, the University has<br>released the results accordingly.<br><br>

This announcement serves as a gentle reminder to the students who have yet to respond to the request for payment of their<br> outstanding fees. We would appreciate very much your kind cooperation to do so by Friday, 15th April 2016 to either settle the<br> payment of fees or apply for the installment plan (which provides for three (3) or less payments), by filling up LOU 1/2016<br> (attached) and submitting it to installment.finance@aeu.edu.my. However, should you require extra installments, you may make a<br> special request with valid reasons and state it in the same form, for due consideration of the management.<br><br>

Failure to do the needful as stated in the para above may affect your status as a student in this University.<br><br>

Thank you for your kind understanding and cooperation.<br><br>

From,<br><br>

REGISTRAR<br><br>
-------------------------------------------------------------------------------------------------------------------------<br><br>
3rd March 2016<br><br>

Dear Student,<br><br>

As you are aware, all registered students are required to pay fees on time in order to avail themselves to <code>student's</code> services.<br><br> 

The University would like to strongly encourage students to make payments on their outstanding fees promptly to ensure continued<br>and uninterrupted quality services.<br><br>

If you are undergoing genuine and valid temporary financial setbacks, please feel free to write in to Finance as soon as possible<br>to seek an amicable solution.<br><br>

Please liaise with Finance Department at installment.finance@aeu.edu.my. (apologies - phone calls will not be entertained),<br> enclosing your proposal on fee settlement by filling up form (LOU 1/2016).<br>
<a href="http://portal.aeu.edu.my:8080/LOU2016_aeu.pdf" download="LOU2016_aeu">Download LOU1/2016 - Letter of Undertaking.</a><br><br>

Thank you for your kind understanding and cooperation.<br>
We apologize for any inconvenience caused.<br><br>

From,<br><br>

REGISTRAR<br><br>



</body>
</html>




