<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
</head>
<?php
session_start();
	if($_SESSION['sid']==session_id())
	{		
	}
	else
	{
		header("location:logout.php");
	}
?>
<body>
    <div>
    		<div class="row">
                <div class="span12">
    			    <h3>2018 AeU Revenue Report: Old and New Students</h3>
                </div>
    		</div>
			<div class="row">

                <div class="span6">
                    <p>
					<?php echo "<h5><a href='logout.php'>Logout after done</a><h5>";?>
                    </p>
                </div>

                <div class="span6">
                    <form class="form-search pull-right" action="<?php echo $_SERVER['PHP_SELF'];?>" method="get">
                        <input type="text" name="query" class="input-medium search-query" value="<?php echo isset($_GET['query'])?$_GET['query']:'';?>">
                        <button type="submit" class="btn">Search</button>
						 </br><h5>Note: Please enter MatricNo, Program name/code ;i.e. C701 or Master</h5>
                    </form>
                </div>

                <div class="span12">
				<table class="table table-striped table-bordered" style="font-size: 13px;">
		              <thead>
		                <tr>
						  <th>School</th>
						  <th>Batch</th>
						  <th>Intake</th>
		                  <th>Name</th>
		                  <th>Matric No</th>
						  <th>TotSub2018</th>
						  <th>TotCrHr18</th>
						  <th>AdminFee</th>
						  <th>RecurFee</th>
						  <th>TuiFee</th>
						  <th>ExamFee</th>
						  <th>Partner</th>
						  <th>Prog.Code</th>
						  <th>Programme</th>	
						 				  
						 
		                </tr>
		              </thead>
		              <tbody>
		              <?php 
                       include 'paginator.php';
                       include 'database.php';
					   
					   $reportYear = 18; $old = 0; $new = 0;
					   $totalStudent= 0; $totalTF = 0; $totalexamFee = 0; $totaladminFee = 0;$totalrecurFee = 0;$totalTF = 0;
                       $pdo = Database::connect();

                       $paginator = new Paginator();
                       
					   $sql = "select count(DISTINCT MATRIXNO) FROM vSubjectEnrollmentview2018";
                       $paginator->totalpage = $pdo->query($sql)->fetchColumn();
					   
					   
					   
					   $sql = "SELECT count(*) FROM  vSubjectEnrollmentview2018";
                       $paginator->paginate($pdo->query($sql)->fetchColumn());
					   
					  
					    

                       $sql = "SELECT SUBSTRING(MATRIXNO, 1, 1) AS School,SUBSTRING(MATRIXNO, 5, 4) AS Intake,STU_NAME,MATRIXNO,COUNT(STU_NAME) as TotalSubject2018, SUM(CREDIT_HR) as TotalCredit2018,SUM(CREDIT_HR*TF_AMOUNT) as TuitionFee, PARTNER,PROG_CODE, PROG_NAME FROM vSubjectEnrollmentview2018 ";

                       $query = isset($_GET['query'])?('%'.$_GET['query'].'%'):'%';
                       $sql .= "WHERE MATRIXNO LIKE :query OR PROG_NAME LIKE :query OR PROG_CODE LIKE :query OR PARTNER LIKE :query ";

                       $start = 0;
                       $length = 5000;
                       $sql .= "GROUP BY STU_NAME ORDER BY DATE_REGISTERED ASC limit :start, :length ";

                       $sth = $pdo->prepare($sql);
                       $sth->bindParam(':start',$start,PDO::PARAM_INT);
                       $sth->bindParam(':length',$length,PDO::PARAM_INT);
                       $sth->bindParam(':query',$query,PDO::PARAM_STR);
                       $sth->execute();
					   echo '<div class="span6">';
					echo	'<p>';
					
					echo	'</p></div>';
					$totalStudent = 0;
					$totalTF = 0;
					$totalrecurFee = 0;
					$totalAdminFee = 0;
					$totalexamFee =0;
					  
	 				   foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $row) {
						         
						        $batch = $row['Intake'];
								if (substr($batch,2,2) == $reportYear) {

									$batch="NEW";
									$adminFee=300;
									$recurFee=0;
									$new +=1;
	
								} 
								else {
									$batch ="OLD";
									$adminFee=0;
									$recurFee=120;
									$old+=1;
									if ( (substr($row['MATRIXNO'],1,1) == '6') or (substr($row['MATRIXNO'],1,1)== '7')) {
										$recurFee=170;}
									}
								
						   		echo '<tr>';
								echo '<td>'. $row['School'] . '</td>';
								echo '<td>'. $batch . '</td>';
								echo '<td>'. $row['Intake'] . '</td>';
								echo '<td>'. $row['STU_NAME'] . '</td>';
							    echo '<td>'. $row['MATRIXNO'] . '</td>';$totalStudent = $totalStudent + 1;
								$ID = substr($row['MATRIXNO'],1,1);
								if (substr($row['MATRIXNO'],1,1) == '6' or substr($row['MATRIXNO'],1,1)=='7') {$examFee=$row['TotalSubject2018']*90;} else {$examFee=$row['TotalSubject2018']*45;}
								echo '<td>'. $row['TotalSubject2018'] . '</td>';
								echo '<td>'. $row['TotalCredit2018'] . '</td>';
								echo '<td>'. $adminFee . '</td>';$totaladminFee = $totaladminFee + $adminFee;
								echo '<td>'. $recurFee . '</td>';$totalrecurFee = $totalrecurFee + $recurFee;
								
								echo '<td>'. $row['TuitionFee']. '</td>';$totalTF = $totalTF + $row['TuitionFee'];
								echo '<td>'. $examFee . '</td>';$totalexamFee = $totalexamFee + $examFee;
								echo '<td>'. $row['PARTNER'] . '</td>';
								echo '<td>'. $row['PROG_CODE'] . '</td>';
								echo '<td>'. $row['PROG_NAME'] . '</td>';
								echo '</tr>';
					   }
					   
					   Database::disconnect();
					  ?>
				      </tbody>
	            </table>
				<div class="span6">
                    <p>
                   
                 <?php
				echo '<table class="table table-striped table-bordered" style="font-size: 13px;">';
				echo '<thead>';
				echo '<tr>';
								
				echo '<th>'. 	"TOTAL STUDENT ". '</th>';;
				echo '<th>'. 	"TOTAL Fee RM".  '</th>';
				echo '<th>'. 	"TOTAL Exam Fee RM".  '</th>';
				echo '<th>'. 	"TOTAL Admission Fee RM".  '</th>';
				echo '<th>'. 	"TOTAL Recurring Fee RM".  '</th>';
				echo '<th>'. 	"TOTAL All RM".  '</th>';
				echo '</tr>';
				echo '<tr>';
				echo '<td>'. 	 $totalStudent  . "</br>New: ". $new. "</br>Old: ".$old.'</td>';;
				echo '<td>'. 	 number_format($totalTF) . '</td>';
				echo '<td>'. 	 number_format($totalexamFee) . '</td>';
				echo '<td>'. 	 number_format($totaladminFee)  . '</td>';
				echo '<td>'. 	 number_format($totalrecurFee) . '</td>';
				echo '<td>'. 	 number_format((int)($totalTF+$totalexamFee+$totaladminFee+$totalrecurFee))  . '</td>';
				
				echo '</tr>';
				echo '</thead>';
				echo '</table>';
                echo $paginator->pageNav();
                ?>
				 </p>
                </div>
                </div>
    	    </div>
    </div> <!-- /container -->
  </body>
</html>
