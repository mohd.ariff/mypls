
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
</head>
<?php

session_start();
	if($_SESSION['sid']==session_id())
	{		
	}
	else
	{
		header("location:logout.php");
	}   
	
?>
<body>
    <div>
    		<div class="row">
                <div class="span12">
    			    <h3>2018 AeU Revenue Report: Old and New Students</h3>
					<h6>Calculation of revenue is based on subject registered income, derived from per credit hour rate.</h6>
					<h6>Plus the Admin, Recurring, and Exam Fee.</h6>
                </div>
    		</div>
			<div class="row">

                <div class="span6">
                    <p>
					<?php echo "<h4><a href='logout.php'>Logout after done</a></h4>"; 
					?>
                    </p>
                </div>

                <div class="span6">
                    <form  action="<?php echo $_SERVER['PHP_SELF'];?>" method="get">
                        <input type="text" name="query" class="input-medium search-query" value="<?php echo isset($_GET['query'])?$_GET['query']:'';?>">
                        <button type="submit" class="btn">Search</button>
						</br><h5>Note: Please enter MatricNo: M or M6 , Program code:i.e. C701 or C7, Partner: Shingo or Sri, Prog name: Master</h5> 
                    </form>
                </div>

                <div class="span12">
				<table class="table table-striped table-bordered" style="font-size: 13px;">
		              <thead>
		                <tr>
						  <th>Partner</th>
						  <th>School</th>
						  <th>Programme</th>
						  <th>Matric No</th>
						  <th>Batch</th>
						  <th>Intake</th>		
						  <th>TotAllSub</th>	
						  <th>TotAllCH</th>						  
						  <th bgcolor="#FF5733">TotSubJan18</th>
						  <th bgcolor="#FF5733">TotCrHrJan18</th>
						  						    
						  <th bgcolor="#FFC300">RegFeeJan18</th>
						  <th bgcolor="#FFC300">RecurFeeJan18</th>
						  <th bgcolor="#FFC300">TuiFeeJan18</th>
						  <th bgcolor="#FFC300">ExamFeeJan18</th>
						  <th bgcolor="#FFC3FF">TotFeeJan18</th>
						  
						  <th bgcolor="#00FF00">TotSubMay18</th>
						  <th bgcolor="#00FF00">TotCrHrMay18</th>
						  						    
						  <th bgcolor="#FFC300">RegFeeMay18</th>
						  <th bgcolor="#FFC300">RecurFeeMay18</th>
						  <th bgcolor="#FFC300">TuiFeeMay18</th>
						  <th bgcolor="#FFC300">ExamFeeMay18</th>
						   <th bgcolor="#FFC3FF">TotFeeMay18</th>
						 
						  <th bgcolor="#A4A5BB">TotSubSep18</th>
						  <th bgcolor="#A4A5BB">TotCrHrSep18</th>
						  
						  <th bgcolor="#FFC300">RegFeeSep18</th>
						  <th bgcolor="#FFC300">RecurFeeSep18</th>
						  <th bgcolor="#FFC300">TuiFeeSep18</th>
						  <th bgcolor="#FFC300">ExamFeeSep18</th>
						   <th bgcolor="#FFC3FF">TotFeeSept18</th>
						  
						  <th bgcolor="#FF5733">TotSubJan19</th>
						  <th bgcolor="#FF5733">TotCrHrJan19</th>
						  <th bgcolor="#FFC300">RegFeeJan19</th>
						  <th bgcolor="#FFC300">RecurFeeJan19</th>
						  <th bgcolor="#FFC300">TuiFeeJan19</th>
						  <th bgcolor="#FFC300">ExamFeeJan19</th>
						   <th bgcolor="#FFC3FF">TotFeeJan19</th>
						  
						  <th bgcolor="#00FF00">TotSubMay19</th>
						  <th bgcolor="#00FF00">TotCrHrMay19</th>
						  <th bgcolor="#FFC300">RegFeeMay19</th>
						  <th bgcolor="#FFC300">RecurFeeMay19</th>
						  <th bgcolor="#FFC300">TuiFeeMay19</th>
						  <th bgcolor="#FFC300">ExamFeeMa19</th>
						   <th bgcolor="#FFC3FF">TotFeeMay19</th>
						  
						  <th bgcolor="#A4A5BB">TotSubSept19</th>
						  <th bgcolor="#A4A5BB">TotCrHrSept19</th>
						  <th bgcolor="#FFC300">RegFeeSept19</th>
						  <th bgcolor="#FFC300">RecurFeeSept19</th>
						  <th bgcolor="#FFC300">TuiFeeSept19</th>
						  <th bgcolor="#FFC300">ExamFeeSept19</th>
						   <th bgcolor="#FFC3FF">TotFeeSept19</th>
						  					 				  
						 
		                </tr>
		              </thead>
		              <tbody> 
 
<?php 


$totalStudent= 0; $totalTF = 0; $totalexamFee = 0; $totaladminFee = 0;$totalrecurFee = 0;$totalTF = 0;$totalOld = 0;$totalNew = 0;
$totalTFsep18 = 0; $totalexamFeesep18 = 0; $totaladminFeesep18 = 0;$totalrecurFeesep18 = 0;$totalTFsep18 = 0;
$totalTFsep19 = 0; $totalexamFeesep19 = 0; $totaladminFeesep19 = 0;$totalrecurFeesep19 = 0;$totalTFsep19 = 0;

include 'database.php';
if ($_SERVER['QUERY_STRING']){ 

$key = $_SERVER['QUERY_STRING'];
echo '<h3>The key :'.$key;echo '</h3>';

	include 'core.php'; 
	include 'core2.php'; 

    $pdo = Database::connect();
	
	
	$sql = "SELECT MATRIXNO,PROG_CODE, PROG_NAME,PARTNER FROM vSubjectEnrollmentview  ";

 $query = isset($_GET['query'])?('%'.$_GET['query'].'%'):'%';
	$sql .= "WHERE MATRIXNO LIKE :query OR STU_NAME LIKE :query OR PROG_NAME LIKE :query OR PARTNER LIKE :query ";       
	$sql .= "";
	$sth = $pdo->prepare($sql);       
	$sth->bindParam(':query',$query,PDO::PARAM_STR);
	$sth->execute();
			 
 $i = 0; $semSub = 0; 
   
	foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $row) {
/*echo '<h3>The key :'.$row['PARTNER'];echo '</h3>';
		 		 			
		echo '<tr>';
		echo '<td>'. $row['PARTNER'] . '</td>';
		//echo '<td>'. getSchool($row['MATRIXNO']) . '</td>';
		echo '<td>'. $row['PROG_CODE'] . '</td>';
			echo '<td>'. $row['MATRIXNO'] . '</td>';
		echo '<td>'. getBatch($row['MATRIXNO'],$reportYear). '</td>';
		echo '<td>'. getIntake($row['MATRIXNO']) . '</td>';
		echo '<td>'. getAllSub($row['MATRIXNO'],$totalRecord,$studentTotalEnrol) . '</td>';
		echo '<td>'. getAllCH($row['MATRIXNO'],$totalRecord,$studentTotalEnrol) . '</td>';

		echo '<td>'. $semSub=getSemSub($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$janSemStart,$janSemEnd) . '</td>';
		echo '<td>'. getSemCH($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$janSemStart,$janSemEnd) . '</td>';
		
		$TAFjan18 = getAdminFee($row['MATRIXNO'],$totalStudent,$studentTotalEnrol,$reportYear,'01','04');
		echo '<td>'. $TAFjan18 . '</td>';
		
		$TrFjan18 = getRecurFee($row['MATRIXNO'],$reportYear,$semSub);
		echo '<td>'. $TrFjan18 . '</td>';
		
		$TFjan18 = getTF($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$janSemStart,$janSemEnd);
		echo '<td>'. $TFjan18. '</td>';
		
		$TeFjan18 = getExamFee($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$janSemStart,$janSemEnd);
		echo '<td>'. $TeFjan18 . '</td>';
		
		$Totjan18 = $TAFjan18+$TrFjan18+$TFjan18+$TeFjan18;
		echo '<td>'. $Totjan18 . '</td>';
		
		echo '<td>'. getSemSub($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$maySemStart,$maySemEnd) . '</td>';
		echo '<td>'. getSemCH($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$maySemStart,$maySemEnd) . '</td>';
		
		$TAFmay18 = getAdminFee($row['MATRIXNO'],$totalStudent,$studentTotalEnrol,$reportYear,'05','08');
		echo '<td>'. $TAFmay18 . '</td>';
		
		$TrFmay18 = getRecurFee($row['MATRIXNO'],$reportYear,$semSub);
		echo '<td>'. $TrFmay18 . '</td>';
		
		$TFmay18 = getTF($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$maySemStart,$maySemEnd);
		echo '<td>'. $TFmay18. '</td>';
		
		$TeFmay18 = getExamFee($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$maySemStart,$maySemEnd);
		echo '<td>'. $TeFmay18 . '</td>';
		
		$Totmay18 = $TAFmay18+$TrFmay18+$TFmay18+$TeFmay18;
		echo '<td>'. $Totmay18 . '</td>';
	
		echo '<td>'. $semSub=getSemSub($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$septSemStart,$septSemEnd) . '</td>';
		echo '<td>'. getSemCH($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$septSemStart,$septSemEnd) . '</td>';
		
		$TAFsept18 = getAdminFee($row['MATRIXNO'],$totalStudent,$studentTotalEnrol,$reportYear,'09','12');
		echo '<td>'. $TAFsept18 . '</td>';
		
		$TrFsept18 = getRecurFee($row['MATRIXNO'],$reportYear,$semSub);
		echo '<td>'. $TrFsept18 . '</td>';
		
		$TFsept18 = getTF($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$septSemStart,$septSemEnd);
	
		echo '<td>'. $TFsept18. '</td>';
		
		$TeFsept18 = getExamFee($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$septSemStart,$septSemEnd);
		echo '<td>'. $TeFsept18 . '</td>';
	
		$Totsept18 = $TAFsept18+$TrFsept18+$TFsept18+$TeFsept18;
		echo '<td>'. $Totsept18 . '</td>';
		
		echo '<td>'. getSemSub($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$janSemStart,$janSemEnd) . '</td>';
		echo '<td>'. getSemCH($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$janSemStart,$janSemEnd) . '</td>';
		
		$TAFjan19 = getAdminFee($row['MATRIXNO'],$totalStudent,$studentTotalEnrol,$reportYear,'01','04');
		echo '<td>'. $TAFjan19 . '</td>';
		$TrFjan19 = getRecurFee($row['MATRIXNO'],$reportYear,$semSub);
		echo '<td>'. $TrFjan19 . '</td>';
		$TFjan19 = getTF($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$janSemStart,$janSemEnd);
		echo '<td>'. $TFjan19 . '</td>';
		$TeFjan19 = getExamFee($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$janSemStart,$janSemEnd);
		echo '<td>'. $TeFjan19 . '</td>';
		$Totjan19 = $TAFjan18+$TrFjan18+$TFjan18+$TeFjan18;
		echo '<td>'. $Totjan19 . '</td>';
		
		echo '<td>'. getSemSub($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$maySemStart,$maySemEnd) . '</td>';
		echo '<td>'. getSemCH($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$maySemStart,$maySemEnd) . '</td>';
		 
		$TAFmay19 = getAdminFee($row['MATRIXNO'],$totalStudent,$studentTotalEnrol,$reportYear,'05','08');
		echo '<td>'. $TAFmay19 . '</td>';
		$TrFmay19 = getRecurFee($row['MATRIXNO'],$reportYear,$semSub);
		echo '<td>'. $TrFmay19 . '</td>';
		$TFmay19 = getTF($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$maySemStart,$maySemEnd);
		echo '<td>'. $TFmay19 . '</td>';
		$TeFmay19 = getExamFee($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$maySemStart,$maySemEnd);
		echo '<td>'. $TeFmay19 . '</td>';
		$Totmay19 =  $TAFmay18+$TrFmay18+$TFmay18+$TeFmay18;
		echo '<td>'. $Totmay19 . '</td>';
		
		echo '<td>'. getSemSub($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$septSemStart,$septSemEnd) . '</td>';
		echo '<td>'. getSemCH($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$septSemStart,$septSemEnd) . '</td>';
		
		$TAFsept19 = getAdminFee($row['MATRIXNO'],$totalStudent,$studentTotalEnrol,$reportYear,'09','12');
		echo '<td>'. $TAFsept19 . '</td>';
		$TrFsept19 = getRecurFee($row['MATRIXNO'],$reportYear,$semSub);
		echo '<td>'. $TrFsept19 . '</td>';
		$TFsept19 = getTF($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$septSemStart,$septSemEnd);
		echo '<td>'. $TFsept19 . '</td>';
		$TeFsept19 = getExamFee($row['MATRIXNO'],$totalRecord,$studentTotalEnrol,$septSemStart,$septSemEnd);
		echo '<td>'. $TeFsept19 . '</td>';
		
		$Totsept19 = $TAFsept18+$TrFsept18+$TFsept18+$TeFsept18;
		echo '<td>'. $Totsept19 . '</td>';
		
		
		echo '</tr>';
*/	
	}
					   
	Database::disconnect();

}
	
?>
		</tbody>
	</table>
		<div class="span6">
			<p>
		   
<?php
				echo '<table class="table table-striped table-bordered" style="font-size: 13px;">';
				echo '<thead>';
				echo '<tr>';
								
				echo '<th>'. 	"TOTAL STUDENT ". '</th>';;
				echo '<th>'. 	"TOTAL Fee RM".  '</th>';
				echo '<th>'. 	"TOTAL Exam Fee RM".  '</th>';
				echo '<th>'. 	"TOTAL Admission Fee RM".  '</th>';
				echo '<th>'. 	"TOTAL Recurring Fee RM".  '</th>';
				echo '<th>'. 	"TOTAL All RM".  '</th>';
				echo '</tr>';
				echo '<tr>';
				echo '<td>'. 	 $totalStudent  . '<br>';echo 'OLD:'. 	 $totalOld  . '<br>';echo 'NEW:'. 	 $totalNew  . '</td>';
				echo '<td>'. 	 number_format($totalTF) . '</td>';
				echo '<td>'. 	 number_format($totalexamFee) . '</td>';
				echo '<td>'. 	 number_format($totaladminFee)  . '</td>';
				echo '<td>'. 	 number_format($totalrecurFee) . '</td>';
				echo '<td>'. 	 number_format((int)($totalTF+$totalexamFee+$totaladminFee+$totalrecurFee))  . '</td>';
				
				echo '</tr>';
				echo '</thead>';
				echo '</table>';
                //echo $paginator->pageNav();

?>
				 </p>
                </div>
                </div>
    	    </div>
    </div> <!-- /container -->
  </body>
</html>
