<?php
$passport=$_GET['passport'];
$con = mysql_connect("10.201.2.100", "b3w0k", "b3w0k43u");
if (!$con)
  {
  die('Could not connect: ' . mysql_error());
  }
$todays_date = date('jS F Y');

mysql_select_db("educate_aeu2", $con);

$pelajar=("select * from VOfferLetterData where passport='$passport'");
	$hasil = mysql_query($pelajar);
	while($row = mysql_fetch_array($hasil))
	{
		
		
		$timestamp = strtotime($row['datec']);
		
		
		require('../fpdf.php');
		class PDF extends FPDF
		{
			
	
			// Page footer
			function Footer()
			{
				// Position at 1.5 cm from bottom
				$this->SetY(-40);
				// Arial italic 8
				$this->SetFont('Arial','',8);
				// Page number
				$this->Cell(0,10,'****Computer Generated Document. No Signature Required*****',0,0,'C');
				$this->Cell(5,5, '', '0', 1, 'C');
				$this->Cell(0,10,'---------------------------------------------------------------------------------',0,0,'C');
				$this->Cell(5,5, '', '0', 1, 'C');
				$this->Cell(0,10,'Admissions & Records, Asia e University, Ground Floor, Main Block, No 4, Jalan Sultan Sulaiman, 50000 Kuala Lumpur',0,0,'C');
				$this->Cell(5,5, '', '0', 1, 'C');
				$this->Cell(0,10,'Tel No: 1-300-300-238 Fax No: 03-27850001 Website: www.aeu.edu.my',0,0,'C');
			}
		}
		if($row['nationalityType'] == '2')
		{			
			// Instanciation of inherited class
			$pdf = new PDF();
			$pdf->AliasNbPages();
			$pdf->AddPage();
			$pdf->SetMargins(25.4,25.4,25.4,25.4);
			$studentName = $row['name'];
			$address1 = $row['address1'];
			$address2 = $row['address2'];
			$address3 = $row['address3'];
			$address4 = $row['address4'];
			$country = $row['country'];
			
			$pdf->Image('../tutorial/aeu_logo.png',155,15,30);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(20,15, '', '0', 1, 'C');
			$pdf->Cell(5,5, $todays_date);
			
			$pdf->Cell(20,8, '', '0', 1, 'C');
			$pdf->SetFont('Arial','B','9'); 
			$pdf->Cell(5,5, $studentName);$pdf->Cell(30,5, '', '0', 1, 'L');
			$pdf->SetFont('Arial','','9'); 
			$pdf->Cell(5,5, $address1);$pdf->Cell(30,5, '', '0', 1, 'L');
			$pdf->Cell(5,5, $address2);$pdf->Cell(30,5, '', '0', 1, 'L');
			$pdf->Cell(5,5, $address3);$pdf->Cell(30,5, '', '0', 1, 'L');
			$pdf->Cell(5,5, $address4);$pdf->Cell(30,5, '', '0', 1, 'L');
			$pdf->Cell(5,5, $country);$pdf->Cell(30,10, '', '0', 1, 'L');
			
			
			
			$pdf->Cell(5,5, $row['salutation']);
			$pdf->Cell(20,8, '', '0', 1, 'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(5,5, $row['title']);
			$pdf->Cell(20,8, '', '0', 1, 'C');
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(5,5, $row['paragraph1']);
			$pdf->Cell(20,8, '', '0', 1, 'C');
			$pdf->Cell(5,5, "The details of the offer are as follows:");
			$pdf->Cell(20,8, '', '0', 1, 'C');
			
			$pdf->SetFont('Arial','','9'); 
			$pdf->Cell(40,5, 'Programme'); $pdf->SetFont('Arial','B',9);$pdf->Cell(70,5, ' : '. $row['program'], '0', 0, 'L'); 
			$pdf->Cell(5,5, '', '0', 1, 'C');
			$pdf->Cell(42,5, ''); $pdf->Cell(70,5, ''. $row['registerNo'], '0', 0, 'L');         
			$pdf->Cell(5,5, '', '0', 1, 'C');$pdf->SetFont('Arial','',9);  	
			$pdf->Cell(40,5, 'Intake');$pdf->SetFont('Arial','B',9);$pdf->Cell(70,5, ' : '. $row['intake'], '0', 0, 'L'); 
			$pdf->Cell(5,5, '', '0', 1, 'C'); $pdf->SetFont('Arial','',9); 
			$pdf->Cell(40,5, 'Commencement Date');$pdf->SetFont('Arial','B',9);$pdf->Cell(70,5, ' : '. date('jS F Y', $timestamp), '0', 0, 'L'); 
			$pdf->Cell(5,5, '', '0', 1, 'C');$pdf->SetFont('Arial','',9);  
			$pdf->Cell(40,5, 'School');$pdf->SetFont('Arial','B',9);$pdf->Cell(70,5, ' : '. $row['faculty'], '0', 0, 'L');
			$pdf->Cell(5,5, '', '0', 1, 'C');$pdf->SetFont('Arial','',9);  
			$pdf->Cell(40,5, 'Duration of Study');$pdf->SetFont('Arial','B',9);$pdf->Cell(70,5, ' : '. $row['duration_of_study'], '0', 0, 'L'); 							
			$pdf->Cell(5,5, '', '0', 1, 'C');$pdf->SetFont('Arial','',9);  
			$pdf->Cell(40,5, 'Programme Structure');$pdf->SetFont('Arial','B',9);$pdf->Cell(30,5, ' : '. $row['learning_method'], '0', 0, 'L');  	
			$pdf->Cell(20,8, '', '0', 1, 'C');
			
			$pdf->SetFont('Arial', '', 9);  
			$text = $row['paragraph2'];
			$pdf->MultiCell(0,5,$text,0,'j');
			
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(20,8, '', '0', 1, 'C');
			$pdf->Cell(5,5, "Yours sincerely,");
			$pdf->Cell(20,8, '', '0', 1, 'C');$pdf->SetFont('Arial','B',9);
			$pdf->Cell(5,5, $row['signature_line1']);$pdf->Cell(30,5, '', '0', 1, 'L');$pdf->SetFont('Arial','',9);
			$pdf->Cell(5,5, $row['signature_line2']);$pdf->Cell(30,5, '', '0', 1, 'L');
			
			
			#output file PDF
			$pdf->Output();
		}
		/*else if($row['nationalityType'] == '1')
		{			
			// Instanciation of inherited class
			$pdf = new PDF();
			$pdf->AliasNbPages();
			$pdf->AddPage();
			$pdf->SetMargins(25.4,25.4,25.4,25.4);
			$studentName = $row['name'];
			$address1 = $row['address1'];
			$address2 = $row['address2'];
			$city = $row['city'];
			
			$pdf->Image('../tutorial/aeu_logo.png',155,15,30);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(20,15, '', '0', 1, 'C');
			$pdf->Cell(5,5, $todays_date);
			
			$pdf->Cell(20,8, '', '0', 1, 'C');
			$pdf->SetFont('Arial','B','9'); 
			$pdf->Cell(5,5, $studentName);$pdf->Cell(30,5, '', '0', 1, 'L');
			$pdf->SetFont('Arial','','9'); 
			$pdf->Cell(5,5, $address1);$pdf->Cell(30,5, '', '0', 1, 'L');
			$pdf->Cell(5,5, $address2);$pdf->Cell(30,5, '', '0', 1, 'L');
			$pdf->Cell(5,5, $city);$pdf->Cell(30,10, '', '0', 1, 'L');
			
			
			
			$pdf->Cell(5,5, $row['salutation']);
			$pdf->Cell(20,8, '', '0', 1, 'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(5,5, $row['title']);
			$pdf->Cell(20,8, '', '0', 1, 'C');
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(5,5, $row['paragraph1']);
			$pdf->Cell(20,8, '', '0', 1, 'C');
			$pdf->Cell(5,5, "The details of the offer are as follows:");
			$pdf->Cell(20,8, '', '0', 1, 'C');
			
			$pdf->SetFont('Arial','','9'); 
			$pdf->Cell(40,5, 'Programme'); $pdf->SetFont('Arial','B',9);$pdf->Cell(70,5, ' : '. $row['program'], '0', 0, 'L'); 
			$pdf->Cell(5,5, '', '0', 1, 'C');
			$pdf->Cell(42,5, ''); $pdf->Cell(70,5, ''. $row['registerNo'], '0', 0, 'L');         
			$pdf->Cell(5,5, '', '0', 1, 'C');$pdf->SetFont('Arial','',9);  	
			$pdf->Cell(40,5, 'Intake');$pdf->SetFont('Arial','B',9);$pdf->Cell(70,5, ' : '. $row['intake'], '0', 0, 'L'); 
			$pdf->Cell(5,5, '', '0', 1, 'C'); $pdf->SetFont('Arial','',9); 
			$pdf->Cell(40,5, 'Commencement Date');$pdf->SetFont('Arial','B',9);$pdf->Cell(70,5, ' : '. "date", '0', 0, 'L'); 
			$pdf->Cell(5,5, '', '0', 1, 'C');$pdf->SetFont('Arial','',9);  
			$pdf->Cell(40,5, 'Learning Center');$pdf->SetFont('Arial','B',9);$pdf->Cell(70,5, ' : '. $row['LC'], '0', 0, 'L');
			$pdf->Cell(5,5, '', '0', 1, 'C');$pdf->SetFont('Arial','',9);  
			$pdf->Cell(40,5, 'Duration of Study');$pdf->SetFont('Arial','B',9);$pdf->Cell(70,5, ' : '. $row['duration_of_study'], '0', 0, 'L'); 							
			$pdf->Cell(5,5, '', '0', 1, 'C');$pdf->SetFont('Arial','',9);  
			$pdf->Cell(40,5, 'Programme Structure');$pdf->SetFont('Arial','B',9);$pdf->Cell(30,5, ' : '. $row['learning_method'], '0', 0, 'L');  	
			$pdf->Cell(20,8, '', '0', 1, 'C');
			
			$pdf->SetFont('Arial', '', 9);  
			$text = $row['paragraph2'];
			$pdf->MultiCell(0,5,$text,0,'j');
			
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(20,8, '', '0', 1, 'C');
			$pdf->Cell(5,5, "Yours sincerely,");
			$pdf->Cell(20,8, '', '0', 1, 'C');$pdf->SetFont('Arial','B',9);
			$pdf->Cell(5,5, $row['signature_line1']);$pdf->Cell(30,5, '', '0', 1, 'L');$pdf->SetFont('Arial','',9);
			$pdf->Cell(5,5, $row['signature_line2']);$pdf->Cell(30,5, '', '0', 1, 'L');
			
			
			#output file PDF
			$pdf->Output();
		}
	}*/
	}
?>


